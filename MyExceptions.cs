﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imenik_projekt_marko_alen
{
    class GetKontaktiException : Exception
    {
        public GetKontaktiException():base("Greška u funkciji za dohvaćanje kontakata.") { }
        public GetKontaktiException(string message) : base(message) { }
    }

    class UpdateKontaktiException : Exception
    {
        public UpdateKontaktiException() : base("Greška u funkciji za update podataka kontakta.") { }
        public UpdateKontaktiException(string message) : base(message) { }
    }

    class AddKontaktiException : Exception
    {
        public AddKontaktiException() : base("Greška u funkciji za dodavanje kontakata.") { }
        public AddKontaktiException(string message) : base(message) { }
    }
    class DeleteKontaktException : Exception
    {
        public DeleteKontaktException() : base("Greška u funkciji za brisanje kontakta.") { }
        public DeleteKontaktException(string message) : base(message) { }
    }
}
