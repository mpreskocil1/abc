﻿namespace Imenik_projekt_marko_alen
{
    partial class EditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKontaktID = new System.Windows.Forms.Label();
            this.btnPohrani = new System.Windows.Forms.Button();
            this.btnOdustani = new System.Windows.Forms.Button();
            this.tBIdKontakta = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.tbMjesto = new System.Windows.Forms.TextBox();
            this.tbUlica = new System.Windows.Forms.TextBox();
            this.tbKucni_broj = new System.Windows.Forms.TextBox();
            this.tbBroj_telefona = new System.Windows.Forms.TextBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblKontaktID
            // 
            this.lblKontaktID.AutoSize = true;
            this.lblKontaktID.Location = new System.Drawing.Point(295, 87);
            this.lblKontaktID.Name = "lblKontaktID";
            this.lblKontaktID.Size = new System.Drawing.Size(81, 17);
            this.lblKontaktID.TabIndex = 3;
            this.lblKontaktID.Text = "ID Kontakta";
            this.lblKontaktID.Click += new System.EventHandler(this.lblKontaktID_Click);
            // 
            // btnPohrani
            // 
            this.btnPohrani.Location = new System.Drawing.Point(129, 512);
            this.btnPohrani.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPohrani.Name = "btnPohrani";
            this.btnPohrani.Size = new System.Drawing.Size(197, 70);
            this.btnPohrani.TabIndex = 4;
            this.btnPohrani.Text = "Pohrani";
            this.btnPohrani.UseVisualStyleBackColor = true;
            this.btnPohrani.Click += new System.EventHandler(this.btnPohrani_Click);
            // 
            // btnOdustani
            // 
            this.btnOdustani.Location = new System.Drawing.Point(463, 512);
            this.btnOdustani.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOdustani.Name = "btnOdustani";
            this.btnOdustani.Size = new System.Drawing.Size(197, 70);
            this.btnOdustani.TabIndex = 5;
            this.btnOdustani.Text = "Odustani";
            this.btnOdustani.UseVisualStyleBackColor = true;
            this.btnOdustani.Click += new System.EventHandler(this.btnOdustani_Click);
            // 
            // tBIdKontakta
            // 
            this.tBIdKontakta.BackColor = System.Drawing.Color.White;
            this.tBIdKontakta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBIdKontakta.Location = new System.Drawing.Point(391, 81);
            this.tBIdKontakta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tBIdKontakta.MaxLength = 150;
            this.tBIdKontakta.Multiline = true;
            this.tBIdKontakta.Name = "tBIdKontakta";
            this.tBIdKontakta.ReadOnly = true;
            this.tBIdKontakta.Size = new System.Drawing.Size(53, 27);
            this.tBIdKontakta.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(296, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Broj telefona";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(296, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Kucni broj";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(296, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 17);
            this.label4.TabIndex = 23;
            this.label4.Text = "Ulica";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(295, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 22;
            this.label3.Text = "Mjesto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 21;
            this.label2.Text = "Prezime";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(295, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Ime";
            // 
            // tbPrezime
            // 
            this.tbPrezime.Location = new System.Drawing.Point(360, 170);
            this.tbPrezime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbPrezime.MaxLength = 20;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(143, 22);
            this.tbPrezime.TabIndex = 15;
            // 
            // tbMjesto
            // 
            this.tbMjesto.Location = new System.Drawing.Point(349, 214);
            this.tbMjesto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbMjesto.MaxLength = 30;
            this.tbMjesto.Name = "tbMjesto";
            this.tbMjesto.Size = new System.Drawing.Size(153, 22);
            this.tbMjesto.TabIndex = 16;
            // 
            // tbUlica
            // 
            this.tbUlica.Location = new System.Drawing.Point(341, 263);
            this.tbUlica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbUlica.MaxLength = 30;
            this.tbUlica.Name = "tbUlica";
            this.tbUlica.Size = new System.Drawing.Size(161, 22);
            this.tbUlica.TabIndex = 17;
            // 
            // tbKucni_broj
            // 
            this.tbKucni_broj.Location = new System.Drawing.Point(375, 313);
            this.tbKucni_broj.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbKucni_broj.MaxLength = 5;
            this.tbKucni_broj.Name = "tbKucni_broj";
            this.tbKucni_broj.Size = new System.Drawing.Size(128, 22);
            this.tbKucni_broj.TabIndex = 18;
            // 
            // tbBroj_telefona
            // 
            this.tbBroj_telefona.Location = new System.Drawing.Point(400, 358);
            this.tbBroj_telefona.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbBroj_telefona.MaxLength = 10;
            this.tbBroj_telefona.Name = "tbBroj_telefona";
            this.tbBroj_telefona.Size = new System.Drawing.Size(103, 22);
            this.tbBroj_telefona.TabIndex = 19;
            // 
            // tbIme
            // 
            this.tbIme.Location = new System.Drawing.Point(332, 121);
            this.tbIme.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbIme.MaxLength = 20;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(171, 22);
            this.tbIme.TabIndex = 14;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 672);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.tbMjesto);
            this.Controls.Add(this.tbUlica);
            this.Controls.Add(this.tbKucni_broj);
            this.Controls.Add(this.tbBroj_telefona);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.tBIdKontakta);
            this.Controls.Add(this.btnOdustani);
            this.Controls.Add(this.btnPohrani);
            this.Controls.Add(this.lblKontaktID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "EditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Uredi kontakt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblKontaktID;
        private System.Windows.Forms.Button btnPohrani;
        private System.Windows.Forms.Button btnOdustani;
        private System.Windows.Forms.TextBox tBIdKontakta;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.TextBox tbMjesto;
        private System.Windows.Forms.TextBox tbUlica;
        private System.Windows.Forms.TextBox tbKucni_broj;
        private System.Windows.Forms.TextBox tbBroj_telefona;
        private System.Windows.Forms.TextBox tbIme;
    }
}