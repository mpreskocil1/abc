﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Imenik_projekt_marko_alen {
    class Kontakti {
        public static List<Kontakt> GetKontakti() {
            List<Kontakt> kontakti = new List<Kontakt>();
            try
            {
                string query = "SELECT * FROM Imenik ORDER BY Ime";

                using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString()))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(query, sqlConnection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    foreach (DataRow redak in ds.Tables[0].Rows)
                    {
                        kontakti.Add(new Kontakt(
                            redak["Id"],
                            redak["Ime"],
                            redak["Prezime"],
                            redak["Mjesto"],
                            redak["Ulica"],
                            redak["Kucni_broj"],
                            redak["Broj_telefona"]
                        ));
                    }
                }
                return kontakti;
            }
            catch (SqlException)
            {
                MessageBox.Show("Greška u bazi podataka.");
                return kontakti;
            }
            catch (GetKontaktiException ex)
            {
                MessageBox.Show(ex.Message);
                return kontakti;
            }
        }
        
        ///////////////////////////////////////////////////////////////////////////
        public static bool AddKontakt(Kontakt kontakt) {
            try
            {
                string query = "INSERT INTO Imenik (Ime, Prezime, Mjesto, Ulica, Kucni_broj, Broj_telefona) VALUES (@Ime, @Prezime, @Mjesto, @Ulica, @Kucni_broj, @Broj_telefona)";

                using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString()))
                {
                    using (SqlCommand command = new SqlCommand(query, sqlConnection))
                    {
                        command.Parameters.AddWithValue("@Ime", kontakt.Ime);
                        command.Parameters.AddWithValue("@Prezime", kontakt.Prezime);
                        command.Parameters.AddWithValue("@Mjesto", kontakt.Mjesto);
                        command.Parameters.AddWithValue("@Ulica", kontakt.Ulica);
                        command.Parameters.AddWithValue("@Kucni_broj", kontakt.Kucni_broj);
                        command.Parameters.AddWithValue("@Broj_telefona", kontakt.Broj_telefona);

                        sqlConnection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }catch (SqlException)
            {
                MessageBox.Show("Greška u bazi podataka.");
                return false;
            }
            catch (AddKontaktiException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        ////////////////////////////////////////////////////////////////////////
        /*
        public static kontakt getkontakt(int id) {
            kontakt kontakt = new kontakt();
            string query = "select * from imenik where id = " + id.tostring();

            using (sqlconnection sqlconnection = new sqlconnection(baza.konekcijskistring())) {
                sqldataadapter adapter = new sqldataadapter(query, sqlconnection);
                dataset ds = new dataset();
                adapter.fill(ds);
                foreach (datarow redak in ds.tables[0].rows) {
                    kontakt.id = (int)redak["id"];
                    kontakt.setime((string)redak["ime"]);
                    kontakt.setprezime((string)redak["prezime"]);
                    kontakt.mjesto = (string)redak["mjesto"];
                    kontakt.ulica = (string)redak["ulica"];
                    kontakt.kucni_broj = (int)redak["kucni_broj"];
                    kontakt.broj_telefona = (string)redak["broj_telefona"];
                }
            }
            return kontakt;
        }
        */
        ////////////////////////////////////////////////////////////////////////

        public static bool UpdateKontakt(Kontakt kontakt) {
            try
            {
                string query = "UPDATE Imenik SET Ime = @Ime, Prezime = @Prezime, Mjesto = @Mjesto, Ulica = @Ulica, Kucni_broj = @Kucni_Broj, Broj_telefona = @Broj_telefona WHERE Id = @Id";
                
                using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString()))
                {
                    using (SqlCommand command = new SqlCommand(query, sqlConnection))
                    {
                        command.Parameters.AddWithValue("@Id", kontakt.Id);
                        command.Parameters.AddWithValue("@Ime", kontakt.Ime);
                        command.Parameters.AddWithValue("@Prezime", kontakt.Prezime);
                        command.Parameters.AddWithValue("@Mjesto", kontakt.Mjesto);
                        command.Parameters.AddWithValue("@Ulica", kontakt.Ulica);
                        command.Parameters.AddWithValue("@Kucni_broj", kontakt.Kucni_broj);
                        command.Parameters.AddWithValue("@Broj_telefona", kontakt.Broj_telefona);

                        sqlConnection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch(SqlException)
            {
                MessageBox.Show("Greška u bazi podataka.");
                return false;
            }
            catch(UpdateKontaktiException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool DeleteKontakt(int id)
        {
            try
            {
                string query = "DELETE FROM Imenik WHERE Id = @Id";

                using (SqlConnection sqlConnection = new SqlConnection(Baza.KonekcijskiString()))
                {
                    using (SqlCommand command = new SqlCommand(query, sqlConnection))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        sqlConnection.Open();
                        int result = command.ExecuteNonQuery();
                        if (result < 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (SqlException)
            {
                MessageBox.Show("Greška u bazi podataka.");
                return false;
            }
            catch (DeleteKontaktException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
