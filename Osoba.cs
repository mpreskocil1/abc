﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imenik_projekt_marko_alen
{
     abstract class Osoba{
        public Osoba(){

        }

        public string Ime { get; protected set; }
        public string Prezime { get; protected set; }
    }

}
