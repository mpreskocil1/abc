﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imenik_projekt_marko_alen
{
    class ZapisZaEditForm {
        public ZapisZaEditForm(object id, object naslovZapisa, object zapis)
        {
            Id = (int)id;
            NaslovZapisa = (string)naslovZapisa;
            ZapisTekst = (string)zapis;
        }
        public ZapisZaEditForm(string naslovZapisa, string zapis)
        {
            NaslovZapisa = naslovZapisa;
            ZapisTekst = zapis;
        }

        public override string ToString()
        {
            return string.Format("{0}", NaslovZapisa);
        }

        public int Id { get; set; }
        public string NaslovZapisa { get; set; }
        public string ZapisTekst { get; set; }
    }

}