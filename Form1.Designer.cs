﻿namespace Imenik_projekt_marko_alen {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lbPrikaz = new System.Windows.Forms.ListBox();
            this.btnDodajKontakt = new System.Windows.Forms.Button();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbBroj_telefona = new System.Windows.Forms.TextBox();
            this.tbKucni_broj = new System.Windows.Forms.TextBox();
            this.tbUlica = new System.Windows.Forms.TextBox();
            this.tbMjesto = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnUrediKontakt = new System.Windows.Forms.Button();
            this.btnIzbrisi = new System.Windows.Forms.Button();
            this.btnSpremiJson = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbPrikaz
            // 
            this.lbPrikaz.FormattingEnabled = true;
            this.lbPrikaz.Location = new System.Drawing.Point(343, 32);
            this.lbPrikaz.Name = "lbPrikaz";
            this.lbPrikaz.Size = new System.Drawing.Size(301, 498);
            this.lbPrikaz.TabIndex = 0;
            // 
            // btnDodajKontakt
            // 
            this.btnDodajKontakt.Location = new System.Drawing.Point(56, 388);
            this.btnDodajKontakt.Name = "btnDodajKontakt";
            this.btnDodajKontakt.Size = new System.Drawing.Size(199, 50);
            this.btnDodajKontakt.TabIndex = 0;
            this.btnDodajKontakt.Text = "Dodaj kontakt";
            this.btnDodajKontakt.UseVisualStyleBackColor = true;
            this.btnDodajKontakt.Click += new System.EventHandler(this.btnDodajKontakt_Click);
            // 
            // tbIme
            // 
            this.tbIme.Location = new System.Drawing.Point(105, 161);
            this.tbIme.MaxLength = 20;
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(129, 20);
            this.tbIme.TabIndex = 1;
            // 
            // tbBroj_telefona
            // 
            this.tbBroj_telefona.Location = new System.Drawing.Point(156, 354);
            this.tbBroj_telefona.MaxLength = 10;
            this.tbBroj_telefona.Name = "tbBroj_telefona";
            this.tbBroj_telefona.Size = new System.Drawing.Size(78, 20);
            this.tbBroj_telefona.TabIndex = 6;
            // 
            // tbKucni_broj
            // 
            this.tbKucni_broj.Location = new System.Drawing.Point(137, 317);
            this.tbKucni_broj.MaxLength = 5;
            this.tbKucni_broj.Name = "tbKucni_broj";
            this.tbKucni_broj.Size = new System.Drawing.Size(97, 20);
            this.tbKucni_broj.TabIndex = 5;
            // 
            // tbUlica
            // 
            this.tbUlica.Location = new System.Drawing.Point(112, 277);
            this.tbUlica.MaxLength = 30;
            this.tbUlica.Name = "tbUlica";
            this.tbUlica.Size = new System.Drawing.Size(122, 20);
            this.tbUlica.TabIndex = 4;
            // 
            // tbMjesto
            // 
            this.tbMjesto.Location = new System.Drawing.Point(118, 237);
            this.tbMjesto.MaxLength = 30;
            this.tbMjesto.Name = "tbMjesto";
            this.tbMjesto.Size = new System.Drawing.Size(116, 20);
            this.tbMjesto.TabIndex = 3;
            // 
            // tbPrezime
            // 
            this.tbPrezime.Location = new System.Drawing.Point(126, 201);
            this.tbPrezime.MaxLength = 20;
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(108, 20);
            this.tbPrezime.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 161);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 201);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 239);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Mjesto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(78, 278);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Ulica";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(78, 320);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Kucni broj";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(78, 356);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Broj telefona";
            // 
            // btnUrediKontakt
            // 
            this.btnUrediKontakt.Location = new System.Drawing.Point(56, 51);
            this.btnUrediKontakt.Name = "btnUrediKontakt";
            this.btnUrediKontakt.Size = new System.Drawing.Size(87, 50);
            this.btnUrediKontakt.TabIndex = 14;
            this.btnUrediKontakt.Text = "Uredi kontakt";
            this.btnUrediKontakt.UseVisualStyleBackColor = true;
            this.btnUrediKontakt.Click += new System.EventHandler(this.btnUrediKontakt_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Location = new System.Drawing.Point(155, 51);
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(100, 50);
            this.btnIzbrisi.TabIndex = 15;
            this.btnIzbrisi.Text = "Izbriši kontakt";
            this.btnIzbrisi.UseVisualStyleBackColor = true;
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // btnSpremiJson
            // 
            this.btnSpremiJson.Location = new System.Drawing.Point(56, 525);
            this.btnSpremiJson.Name = "btnSpremiJson";
            this.btnSpremiJson.Size = new System.Drawing.Size(107, 30);
            this.btnSpremiJson.TabIndex = 16;
            this.btnSpremiJson.Text = "Spremi u JSON";
            this.btnSpremiJson.UseVisualStyleBackColor = true;
            this.btnSpremiJson.Click += new System.EventHandler(this.btnSpremiJson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 580);
            this.Controls.Add(this.btnSpremiJson);
            this.Controls.Add(this.btnIzbrisi);
            this.Controls.Add(this.btnUrediKontakt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.tbMjesto);
            this.Controls.Add(this.tbUlica);
            this.Controls.Add(this.tbKucni_broj);
            this.Controls.Add(this.tbBroj_telefona);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.btnDodajKontakt);
            this.Controls.Add(this.lbPrikaz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Imenik";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbPrikaz;
        private System.Windows.Forms.Button btnDodajKontakt;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbBroj_telefona;
        private System.Windows.Forms.TextBox tbKucni_broj;
        private System.Windows.Forms.TextBox tbUlica;
        private System.Windows.Forms.TextBox tbMjesto;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnUrediKontakt;
        private System.Windows.Forms.Button btnIzbrisi;
        private System.Windows.Forms.Button btnSpremiJson;
    }
}

