﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Imenik_projekt_marko_alen
{
    public partial class EditForm : Form {
        private Form1 parent;
        private string[] words;
        public EditForm(Form1 Form1, string[] words) {
            parent = Form1;
            this.words = words;
            InitializeComponent();
            this.ShowInTaskbar = false;
            tBIdKontakta.Text = this.words[0];
            tbIme.Text = this.words[2];
            tbPrezime.Text = this.words[4];
            tbMjesto.Text = this.words[6];
            tbUlica.Text = this.words[8];
            tbKucni_broj.Text = this.words[10];
            tbBroj_telefona.Text = this.words[12];
        }

        /*
        private void bntPohrani_Click(object sender, EventArgs e) {
            var zapis = new ZapisZaEditForm(tbKontakt.Text.ToString(), tbOpis.Text.ToString());
            if(!ZapisZaEditForm.AzurirajZapis(zapis, this.kontakt))
            {
                MessageBox.Show("Greška");
            }
            else
            {
                this.Close();
                parent.OsvjeziListuZapisa();
                parent.OsvjeziOpisZapisa(zapis.NaslovZapisa);
            }

        }
        */
        private void btnOdustani_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void lblKontaktID_Click(object sender, EventArgs e)
        {

        }

        private void btnPohrani_Click(object sender, EventArgs e)
        {
            Kontakti.UpdateKontakt(new Kontakt(Int32.Parse(tBIdKontakta.Text), tbIme.Text, tbPrezime.Text, tbMjesto.Text, tbUlica.Text, tbKucni_broj.Text, tbBroj_telefona.Text));
            this.Close();
            parent.RefreshKontakti();
        }

        /*
        private void EditForm_Load(object sender, EventArgs e) {
           textBox1.Text = this.kontakt;
            tbOpis.Text = ZapisZaEditForm.DohvatiOpis(kontakt);

        }*/
    }
}
