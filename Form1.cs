﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using static System.Net.WebRequestMethods;

namespace Imenik_projekt_marko_alen {
    public partial class Form1 : Form {
        Thread dretvaRefresh;

        public Form1() {
            // Dopušta korištenje elemata koji su stvoreni u nekoj drugoj dretvi
            ListBox.CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            dretvaRefresh = new Thread(new ThreadStart(RefreshKontakti));
            dretvaRefresh.Name = "Refresh kontakata";
        }

        private void Form1_Load(object sender, EventArgs e) {
            dretvaRefresh.Start();
            //RefreshKontakti();
        }
        
        //Pozivanje baze i spremanje podataka o kontaktu
        private void btnDodajKontakt_Click(object sender, EventArgs e) {
            if (!Regexp(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", tbIme))
            {
                MessageBox.Show("Molimo unesite ispravno ime.");
                return;
            }
            if (!Regexp(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", tbPrezime))
            {
                MessageBox.Show("Molimo unesite ispravno prezime.");
                return;
            }
            if (!Regexp(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", tbMjesto))
            {
                MessageBox.Show("Molimo unesite ispravno mjesto.");
                return;
            }
            if (!Regexp(@"^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$", tbUlica))
            {
                MessageBox.Show("Molimo unesite ispravnu ulicu.");
                return;
            }
            if (!Regexp(@"^[0-9]", tbKucni_broj))
            {
                MessageBox.Show("Molimo unesite ispravan kućni broj.");
                return;
            }
            if (!Regexp(@"^[0-9]", tbBroj_telefona))
            {
                MessageBox.Show("Molimo unesite ispravan broj telefona.");
                return;
            }
            DodajKontakt();
        }

        //Osvježavanje liste za prikaz kontakata
        public void RefreshKontakti() {
            // Thread.Sleep(5000);
            lbPrikaz.Items.Clear();
            var kontakti = Kontakti.GetKontakti();
            foreach (var kontakt in kontakti) {
                string temp = kontakt.Id + ", " + kontakt.Ime + ", " + kontakt.Prezime + ", " + kontakt.Mjesto + ", " + kontakt.Ulica + ", " + kontakt.Kucni_broj + ", " + kontakt.Broj_telefona;
                string trim = temp.Replace(" ", "");
                string trimTrimed = trim.Replace(",", ", ");
                lbPrikaz.Items.Add(trimTrimed);
            }
        }
        
        private void btnUrediKontakt_Click(object sender, EventArgs e)
        {
          if(lbPrikaz.SelectedIndex == -1)
            {
               MessageBox.Show("Molimo odaberite neki od kontakata.");
            }
            else
            {
                string text = lbPrikaz.GetItemText(lbPrikaz.SelectedItem);
                char[] charsToTrim = { ',', '.', ' ' };
                string[] words = text.Split(charsToTrim);
                EditForm editForm = new EditForm(this, words);
                editForm.Show();
                /*words[words.Length-1]*/
            }
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            if (lbPrikaz.SelectedIndex == -1)
            {
                MessageBox.Show("Molimo odaberite neki od kontakata.");
            }
            else
            {
                string text = lbPrikaz.GetItemText(lbPrikaz.SelectedItem);
                char[] charsToTrim = { ',', '.', ' ' };
                string[] words = text.Split(charsToTrim);
                Kontakti.DeleteKontakt(Int32.Parse(words[0]));
                RefreshKontakti();
            }
        }

        private void btnSpremiJson_Click(object sender, EventArgs e)
        {
            List<Kontakt> listaKontakata = new List<Kontakt>();
            var kontakti = Kontakti.GetKontakti();
            foreach (var kontakt in kontakti)
            {
                listaKontakata.Add(new Kontakt(kontakt.Id, kontakt.Ime, kontakt.Prezime, kontakt.Mjesto, kontakt.Ulica, kontakt.Kucni_broj, kontakt.Broj_telefona));
                Console.WriteLine(kontakt.Id.ToString() + kontakt.Ime.ToString() + kontakt.Prezime.ToString() + kontakt.Mjesto + kontakt.Ulica + kontakt.Kucni_broj + kontakt.Broj_telefona);
            }
            FileStream sb = new FileStream("kontakti.json", FileMode.OpenOrCreate);
            var jsonToWrite = JsonConvert.SerializeObject(kontakti, Formatting.Indented);
            using (var writer = new StreamWriter(sb))
            {
                writer.Write(jsonToWrite);
            }
        }

        private bool Regexp (string re, TextBox tb)
        {
            Regex regex = new Regex(re);
            if (regex.IsMatch(tb.Text))
            {
                return true;
            }
            return false;
        }

        private void DodajKontakt()
        {
            Kontakti.AddKontakt(new Kontakt(0, tbIme.Text, tbPrezime.Text, tbMjesto.Text, tbUlica.Text, tbKucni_broj.Text, tbBroj_telefona.Text));
            lbPrikaz.Items.Clear();
            RefreshKontakti();
        }
    }
}
