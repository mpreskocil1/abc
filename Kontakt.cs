﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imenik_projekt_marko_alen {
    class Kontakt : Osoba {

        public Kontakt(){
        }
        
        public Kontakt(int id, string ime, string prezime, string mjesto, string ulica, string kucni_broj, string broj_telefona) {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Mjesto = mjesto;
            Ulica = ulica;
            Kucni_broj = Int32.Parse(kucni_broj);
            Broj_telefona = broj_telefona;
        }

        public Kontakt(object id, object ime, object prezime, object mjesto, object ulica, object kucni_broj, object broj_telefona) {
            Id = (int)id;
            Ime = (string)ime;
            Prezime = (string)prezime;
            Mjesto = (string)mjesto;
            Ulica = (string)ulica;
            Kucni_broj = (int)kucni_broj;
            Broj_telefona = (string)broj_telefona;            
        }
        
        public override string ToString() {
            return Ime;
        }

        public void setIme(string ime)
        {
            this.Ime = ime;
        }

        public void setPrezime(string prezime)
        {
            this.Prezime = prezime;
        }
        public int Id { get; set; }

        public string Mjesto { get; set; }
        public string Ulica { get; set; }
        public int Kucni_broj { get; set; }
        public string Broj_telefona { get; set; }      

    }
}
